$(window).on("load", function() {
    if($('.loader').length){
        $('.loader').delay(999).fadeOut(300);
    }

})
/**
 * Easy selector helper function
 */
const select = (el, all = false) => {
    el = el.trim()
    if (all) {
        return [...document.querySelectorAll(el)]
    } else {
        return document.querySelector(el)
    }
}

/**
 * Easy event listener function
 */
const on = (type, el, listener, all = false) => {
    let selectEl = select(el, all)
    if (selectEl) {
        if (all) {
            selectEl.forEach(e => e.addEventListener(type, listener))
        } else {
            selectEl.addEventListener(type, listener)
        }
    }
}

/**
 * Easy on scroll event listener
 */
const onscroll = (el, listener) => {
    el.addEventListener('scroll', listener)
}

/**
 * Navbar links active state on scroll
 */
let navbarlinks = select('#navbar .scrollto', true)
const navbarlinksActive = () => {
    let position = window.scrollY + 200
    navbarlinks.forEach(navbarlink => {
        if (!navbarlink.hash) return
        let section = select(navbarlink.hash)
        if (!section) return
        if (position >= section.offsetTop && position <= (section.offsetTop + section.offsetHeight)) {
            navbarlink.classList.add('active')
        } else {
            navbarlink.classList.remove('active')
        }
    })
}
window.addEventListener('load', navbarlinksActive)
onscroll(document, navbarlinksActive)

/**
 * Scrolls to an element with header offset
 */
const scrollto = (el) => {
    let header = select('#header')
    let offset = header.offsetHeight

    let elementPos = select(el).offsetTop
    window.scrollTo({
        top: elementPos - offset,
        behavior: 'smooth'
    })
}

/**
 * Toggle .header-scrolled class to #header when page is scrolled
 */
let selectHeader = select('#header')
if (selectHeader) {
    const headerScrolled = () => {
        if (window.scrollY > 100) {
            selectHeader.classList.add('header-scrolled')
        } else {
            selectHeader.classList.remove('header-scrolled')
        }
    }
    window.addEventListener('load', headerScrolled)
    onscroll(document, headerScrolled)
}

/**
 * Mobile nav toggle
 */
on('click', '.mobile-nav-toggle', function(e) {
    select('#navbar').classList.toggle('navbar-mobile')
    this.classList.toggle('bi-list')
    this.classList.toggle('bi-x')
})

/**
 * Mobile nav dropdowns activate
 */
on('click', '.navbar .dropdown > a', function(e) {
    if (select('#navbar').classList.contains('navbar-mobile')) {
        e.preventDefault()
        this.nextElementSibling.classList.toggle('dropdown-active')
    }
}, true)

/**
 * Scroll with offset on links with a class name .scrollto
 */
on('click', '.scrollto', function(e) {
    if (select(this.hash)) {
        e.preventDefault()

        let navbar = select('#navbar')
        if (navbar.classList.contains('navbar-mobile')) {
            navbar.classList.remove('navbar-mobile')
            let navbarToggle = select('.mobile-nav-toggle')
            navbarToggle.classList.toggle('bi-list')
            navbarToggle.classList.toggle('bi-x')
        }
        scrollto(this.hash)
    }
}, true)

/**
 * Scroll with ofset on page load with hash links in the url
 */
window.addEventListener('load', () => {
    if (window.location.hash) {
        if (select(window.location.hash)) {
            scrollto(window.location.hash)
        }
    }
});

var grid =  $('.grid').isotope({
    // options
    itemSelector: '.element-item',
    layoutMode: 'fitRows'
});

$('.filter-button-group').on( 'click', 'button', function() {
    var filterValue = $( this ).attr('data-filter');
    $(".tab-group").find(".tab-selected").removeClass("tab-selected");
    $(this).addClass("tab-selected");
    console.log(filterValue);
    grid.isotope({ filter: filterValue });
});

$(document).ready(function(){
    $("#clientele-owl-carousel").owlCarousel({
        autoplay : true,
        autoplayTimeout : '2999',
        margin:14,
        responsive : {
            0:{
                items:1,
            },
            600:{
                items:2,
                loop:true
            },
            1000:{
                items:3,
                loop:true,
            },
            responsiveRefreshRate : '99',
        },
        loop : true
    });

    $("#landing-owl-carousel").owlCarousel({
        items:1,
        autoplay : true,
        autoplayTimeout : '2999',
        loop: true,
        center: true,
        smartSpeed:450
    });

    $("#office-space-owl-carousel").owlCarousel({
        autoplay : true,
        autoplayTimeout : '2999',
        margin:14,
        responsive : {
            0:{
                items:1,
            },
            600:{
                items:2,
                loop:true
            },
            1000:{
                items:3,
                loop:true,
            },
            responsiveRefreshRate : '99',
        },
        loop : true
    });

    /*START: JQUERY VALIDATION*/

    $("#send-us-message-form").validate({
        rules:{
            "name": {
                required: true,
                minlength: 2,
            },
            "email": {
                required: true,
                email: true,
            },
            "subject": {
                required: true,
                minlength: 2,
            },
            "message": {
                required: true,
                minlength: 2,
            },
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    /*END: JQUERY VALIDATION*/

});

$(window).on("load", function () {
    $("#about-us-section").css('opacity', 0);
    $("#services-section").css('opacity', 0);
    $("#clientele-section").css('opacity', 0);
    $("#contact-us-section").css('opacity', 0);
    $("#office-space-section").css('opacity', 0);
});

$("#about-us-section").waypoint(function () {
    $("#about-us-section").addClass("animate__animated animate__fadeInUp");
}, {offset: '40%'});

$("#services-section").waypoint(function () {
    $("#services-section").addClass("animate__animated animate__fadeInUp");
}, {offset: '60%'});

$("#clientele-section").waypoint(function () {
    $("#clientele-section").addClass("animate__animated animate__fadeInUp");
}, {offset: '60%'});

$("#contact-us-section").waypoint(function () {
    $("#contact-us-section").addClass("animate__animated animate__fadeInUp");
}, {offset: '60%'});

$("#office-space-section").waypoint(function () {
    $("#office-space-section").addClass("animate__animated animate__fadeInUp");
}, {offset: '60%'});

$('[name="apply-job-input"]').on('change', function (){
    var resumeInput = $('#resume-input');
    if(!$('[name="apply-job-input"]')[0].checked){
        resumeInput.attr('disabled', 'true');
        resumeInput.rules("remove");
    }else{
        resumeInput.removeAttr('disabled');
        resumeInput.rules("add", {
            required: true,
            extension: "jpg|jpeg|png|pdf|doc|docx",
            messages: {
                extension: "Please enter valid extension files (.jpg .jpeg .png .pdf .doc .docx)"
            },
        });
    }
})
