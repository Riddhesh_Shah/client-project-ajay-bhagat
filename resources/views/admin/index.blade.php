<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Bhagat Ajay & Co.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{--    <link rel="stylesheet" href="http://fonts.cdnfonts.com/css/product-sans-infanity">--}}
    <link rel="stylesheet" href="{{asset('vendor/bootstrap-5.0.2-dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/DataTables-1.12.1/datatables.css')}}">
    <link rel="stylesheet" href="{{ asset('css/styles.css')  }}">
    <style>
        .header {
            width: 100%;
            background-color: var(--secondary_color);
            height: 3rem;
            color: white;
            box-shadow: 0 4px 15px rgb(58 112 191 / 25%);
        }

        .header h5 {
            font-size: 1.7rem;
        }
    </style>
</head>
<body>
    <div class="header d-flex align-items-center">
        <div class="container">
            <div class="d-flex justify-content-between">
                <h5>DashBoard</h5>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <button type="submit" class="btn btn-light">Logout</button>
                </form>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mt-4">
            <div class="col-md-12">
                <div class="card p-4">
                    <div class="card-title">
                        <h5>Messages</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="client-messages" class="display table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Subject</th>
                                        <th>Message</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mt-3">
                <div class="card p-4">
                    <div class="card-title">
                        <h5>Work With Us Messages</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="work-with-us-messages" class="display table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Subject</th>
                                        <th>Message</th>
                                        <th>Resume</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<footer>
    <div class="lower-footer bg_secondary text_white text-center py-3 mt-4">
        <span>&copy; Copyright Bhagat Ajay & Co. All Rights Reserved</span>
        <small class="d-block mt-2">Designed by Riddhesh Shah & Soham Karalkar</small>
    </div>
</footer>
<script src="{{asset('vendor/bootstrap-5.0.2-dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('vendor/jQuery/jquery.min.js')}}"></script>
<script src="{{asset('vendor/owl-carousel/dist/owl.carousel.min.js')}}"></script>
<script src="{{asset('vendor/isotope/isotope.min.js')}}"></script>
<script src="{{asset('vendor/waypoints/lib/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('vendor/jquery-validate/jquery-validate.min.js')}}"></script>
<script rel="stylesheet" src="{{asset('vendor/DataTables-1.12.1/datatables.js')}}"></script>
<script src="{{asset('js/scripts.js')}}"></script>
<script>
    $('#client-messages').DataTable( {
        ajax: {
            url: '{{route("admin.getClientMessages")}}',
            method: 'GET'
        },
        columns: [
            {'data':'name'},
            {'data':'email'},
            {'data':'subject'},
            {'data':'message'}
        ]
    } );

    $('#work-with-us-messages').DataTable( {
        ajax: {
            url: '{{route("admin.getWorkWithUsMessages")}}',
            method: 'GET'
        },
        columns: [
            {'data':'name'},
            {'data':'email'},
            {'data':'subject'},
            {'data':'message'},
            {
                'data': 'resume',
                'render' : function (dataField) {
                    var url = "{{route("admin.download", "#")}}";
                    url = url.replace("#", dataField);
                    return '<a href="'+url+'" class="btn btn-sm btn-primary" target="_blank">Resume</a>';
                }
            }
        ]
    } );
</script>
</html>
