<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Bhagat Ajay & Co.</title>
    <link rel="icon" href="{{asset('images/ca-logo.gif')}}" type="image/gif" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('vendor/bootstrap-5.0.2-dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/bootstrap-icons/bootstrap-icons.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/fontawesome-free-5.15.4-web')}}">
    <link rel="stylesheet" href="{{asset('vendor/owl-carousel/dist/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/owl-carousel/dist/assets/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/boxicons/css/boxicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/animate.css/animate.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/styles.css')  }}">
</head>
<body>
<div class="loader">
    <div class="cell">
        <div class="wrapper">
            <div class="spinner spinner4"></div>
        </div>
    </div>
</div>
<header id="header" class="fixed-top bg_secondary">
    <div class="container d-flex align-items-center">

        <img src="{{asset('images/ca-logo.gif')}}" alt="ca logo" id="ca-logo" width="50px" class="mx-2">
        <div class="me-auto">
            <h1 class="logo text_white">Bhagat Ajay & Co.</h1>
            <small class="text_white">Chartered Accountants</small>
        </div>

        <nav id="navbar" class="navbar">
            <ul class="nav-link-list">
                <li><a class="nav-link scrollto nav-link-active hover-underline-animation" href="#hero-section">Home</a></li>
                <li><a class="nav-link scrollto hover-underline-animation" href="#about-us-section">About Us</a></li>
                <li><a class="nav-link scrollto hover-underline-animation" href="#services-section">Our Services</a></li>
                <li><a class="nav-link scrollto hover-underline-animation" href="#clientele-section">Our Clientele</a></li>
                <li><a class="nav-link scrollto hover-underline-animation" href="#office-space-section">Our Office Space</a></li>
                <li><a class="nav-link scrollto hover-underline-animation" href="#contact-us-section">Contact Us</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav>
    </div>
</header>
<div class="margin-header">
    <div id="hero-section" class="mb-5 pb-5">
        <div class="container">

            <div class="row">
                <div class="col-md-6 text-center justify-content-center mt-3">
                    <div class="hero-image-container d-flex justify-content-center">
                        <div id="landing-owl-carousel" class="owl-carousel">
                            <div class="image-holder">
                                <img src="{{asset('images/hero-section-carousel/img1.svg')}}" class="" alt="person looking at data">
                            </div>
                            <div class="image-holder">
                                <img src="{{asset('images/hero-section-carousel/img2.svg')}}" alt="laptop with statistics" class="">
                            </div>
                            <div class="image-holder">
                                <img src="{{asset('images/hero-section-carousel/img3.svg')}}" alt="person looking at charts" class="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 text-center align-self-center mt-3">
                    <h1 id="hero-tagline">Calculating the best <span class="text_secondary">opportunities</span> for you
                    </h1>
                </div>

            </div>
        </div>
    </div>
    <div id="about-us-section">
        <div class="container">
            <div class="about-us-title-container">
                <h2 id="about-us-title" class="text_secondary text-center pt-4">
                    About Us
                </h2>
            </div>
            <div class="row mt-5">
                <div class="col-md-6">
                    <div class="">
                        <h4 class="text_secondary">Our Mission</h4>
                        <p>With the power of the BAAC’s talent pool, we remain committed to excellence & deliver value
                            to our Clients. “Quality is priority” is the ultimate aspiration. Our vision is to nurture
                            a professional organization of repute which is competitive, dynamic & focused in the areas
                            of its operations.</p>
                    </div>
                    <div class="mt-5">
                        <div class="card mb-3 mx-auto" id="owner-card">
                            <div class="row g-0">
                                <div class="col-md-4">
                                    <img src="{{asset('images/ajay-bhagat.png')}}" class="img-fluid rounded-start" alt="ca ajay bhagat"
                                         id="owner-image">
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <h5 class="card-title"><strong>CA Ajay K. Bhagat</strong></h5>
                                        <span class="small"><strong>FCA, LLB, M.Com</strong></span>
                                        <p class="card-text"><small class="text-muted">Partner</small></p>
                                        <p class="card-text">Ajay K. Bhagat is a fellow member of the Institute of
                                            Chartered Accountants of India, & has more than 15 years of experience in
                                            the profession of Chartered Accountancy.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="about-us-image-container text-center">
                        <img src="{{asset('images/team.jpg')}}" alt="office team" id="about-us-image">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="services-section">
        <div class="services-title-container">
            <h2 id="services-title" class="text_secondary text-center pt-4">
                Our Services
            </h2>
        </div>
        <div class="container">
            <div class="accordion-list">
                <ul>
                    <li>
                        <a data-bs-toggle="collapse" data-bs-target="#accordion-list-1"
                           class="collapsed"><span class="bi bi-chevron-double-right"></span> Direct Taxation <i
                                class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                        <div id="accordion-list-1" class="collapse" data-bs-parent=".accordion-list">
                            <ul class="service-sub-list">
                                <li><span class="bi bi-arrow-right"></span>Tax Advisory Services</li>
                                <li><span class="bi bi-arrow-right"></span>Tax Structuring & Planning</li>
                                <li><span class="bi bi-arrow-right"></span>Taxation of NRIs & Foreign companies</li>
                                <li><span class="bi bi-arrow-right"></span>Representation before Tax Authorities</li>
                                <li><span class="bi bi-arrow-right"></span>Representation before Appellate Authorities</li>
                                <li><span class="bi bi-arrow-right"></span>Assisting in obtaining various certificates from authorities</li>
                                <li><span class="bi bi-arrow-right"></span>Assisting Trust in obtaining 80G, 12AA certificate Appearing before ITAT</li>
                                <li><span class="bi bi-arrow-right"></span>Liaisoning with Senior Tax Counsels</li>
                                <li><span class="bi bi-arrow-right"></span>Tax compliance Services</li>
                                <li><span class="bi bi-arrow-right"></span>Other Allied services</li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a data-bs-toggle="collapse" data-bs-target="#accordion-list-2"
                           class="collapsed"><span class="bi bi-chevron-double-right"></span> Indirect Taxation <i
                                class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                        <div id="accordion-list-2" class="collapse" data-bs-parent=".accordion-list">
                            <ul class="service-sub-list">
                                <li><span class="bi bi-arrow-right"></span>Registration with GST Authorities</li>
                                <li><span class="bi bi-arrow-right"></span>Advisory including determination of valuation, rates, place & time of supply</li>
                                <li><span class="bi bi-arrow-right"></span>Tax Compliance Services like filling of Returns etc</li>
                                <li><span class="bi bi-arrow-right"></span>Representation before Authorities</li>
                                <li><span class="bi bi-arrow-right"></span>Refund application before Authorities</li>
                                <li><span class="bi bi-arrow-right"></span>PTRC & PTEC related services</li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a data-bs-toggle="collapse" data-bs-target="#accordion-list-3"
                           class="collapsed"><span class="bi bi-chevron-double-right"></span> Audit & Assurance <i
                                class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                        <div id="accordion-list-3" class="collapse" data-bs-parent=".accordion-list">
                            <ul class="service-sub-list">
                                <li><span class="bi bi-arrow-right"></span>Statutory Audit</li>
                                <li><span class="bi bi-arrow-right"></span>Trust & Cooperative Societies Audits</li>
                                <li><span class="bi bi-arrow-right"></span>Bank Audits</li>
                                <li><span class="bi bi-arrow-right"></span>Tax Audits under Income Tax Act</li>
                                <li><span class="bi bi-arrow-right"></span>GST Audits under GST Act</li>
                                <li><span class="bi bi-arrow-right"></span>Internal Audits</li>
                                <li><span class="bi bi-arrow-right"></span>Voluntary Audits</li>
                                <li><span class="bi bi-arrow-right"></span>Compliance Audits</li>
                                <li><span class="bi bi-arrow-right"></span>Special Investigation Audits</li>
                                <li><span class="bi bi-arrow-right"></span>Financial Review</li>
                                <li><span class="bi bi-arrow-right"></span>Stock & Fixed Asset Verification</li>
                                <li><span class="bi bi-arrow-right"></span>Management & Proprietary Audit</li>
                                <li><span class="bi bi-arrow-right"></span>Risk Advisory</li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a data-bs-toggle="collapse" data-bs-target="#accordion-list-4"
                           class="collapsed"><span class="bi bi-chevron-double-right"></span> Legal Services <i class="bx bx-chevron-down icon-show"></i><i
                                class="bx bx-chevron-up icon-close"></i></a>
                        <div id="accordion-list-4" class="collapse" data-bs-parent=".accordion-list">
                            <ul class="service-sub-list">
                                <li><span class="bi bi-arrow-right"></span>Drafting of MOU/ Agreements</li>
                                <li><span class="bi bi-arrow-right"></span>Drafting of Wills/ Gift Deeds</li>
                                <li><span class="bi bi-arrow-right"></span>Drafting of MOA / AOA</li>
                                <li><span class="bi bi-arrow-right"></span>Drafting of Partnership deed</li>
                                <li><span class="bi bi-arrow-right"></span>Drafting of HUF Agreements</li>
                                <li><span class="bi bi-arrow-right"></span>Other legal drafting services</li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a data-bs-toggle="collapse" data-bs-target="#accordion-list-5"
                           class="collapsed"><span class="bi bi-chevron-double-right"></span> Financial Advisory Services <i
                                class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                        <div id="accordion-list-5" class="collapse" data-bs-parent=".accordion-list">
                            <ul class="service-sub-list">
                                <li><span class="bi bi-arrow-right"></span>Project Reports</li>
                                <li><span class="bi bi-arrow-right"></span>Preparation of CMA reports</li>
                                <li><span class="bi bi-arrow-right"></span>Assisting in obtaining MSME loans </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a data-bs-toggle="collapse" data-bs-target="#accordion-list-6"
                           class="collapsed"><span class="bi bi-chevron-double-right"></span> Corporate Restructuring Services <i
                                class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                        <div id="accordion-list-6" class="collapse" data-bs-parent=".accordion-list">
                            <ul class="service-sub-list">
                                <li><span class="bi bi-arrow-right"></span>Business Set up Services</li>
                                <li><span class="bi bi-arrow-right"></span>SEBI Compliance</li>
                                <li><span class="bi bi-arrow-right"></span>Listing Agreements & Compliance</li>
                                <li><span class="bi bi-arrow-right"></span>Due Diligence</li>
                                <li><span class="bi bi-arrow-right"></span>Corporate Structuring</li>
                                <li><span class="bi bi-arrow-right"></span>ROC related Services</li>
                                <li><span class="bi bi-arrow-right"></span>Start-Up Advisory</li>
                                <li><span class="bi bi-arrow-right"></span>Start-Up Registration</li>
                                <li><span class="bi bi-arrow-right"></span>MSME Registration</li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a data-bs-toggle="collapse" data-bs-target="#accordion-list-7"
                           class="collapsed"><span class="bi bi-chevron-double-right"></span> Transaction Advisory Services <i
                                class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                        <div id="accordion-list-7" class="collapse" data-bs-parent=".accordion-list">
                            <ul class="service-sub-list">
                                <li><span class="bi bi-arrow-right"></span>Valuations of Equity & other instrument</li>
                                <li><span class="bi bi-arrow-right"></span>Structuring & Execution</li>
                                <li><span class="bi bi-arrow-right"></span>Personal Financial Advisory</li>
                                <li><span class="bi bi-arrow-right"></span>Research Reports</li>
                                <li><span class="bi bi-arrow-right"></span>Mergers / demergers & Acquisitions</li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a data-bs-toggle="collapse" data-bs-target="#accordion-list-8"
                           class="collapsed"><span class="bi bi-chevron-double-right"></span> Real Estate Related Services <i
                                class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                        <div id="accordion-list-8" class="collapse" data-bs-parent=".accordion-list">
                            <ul class="service-sub-list">
                                <li><span class="bi bi-arrow-right"></span>Registration of Projects & Agents</li>
                                <li><span class="bi bi-arrow-right"></span>Consultancy relating to Compliance</li>
                                <li><span class="bi bi-arrow-right"></span>Audit of Financials in accordance with RERA Regulations</li>
                                <li><span class="bi bi-arrow-right"></span>Filing of Quarterly Updates</li>
                                <li><span class="bi bi-arrow-right"></span>Representing before RERA Authorities & Appellate Tribunal</li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="clientele-section">
        <div class="container">
            <div class="clientele-title-container">
                <h2 id="clientele-title" class="text_secondary text-center pt-4">
                    Our Clientele
                </h2>
            </div>
            <div id="clientele-owl-carousel" class="owl-carousel mt-5">
                <div class="cl-card">
                    <div class="cl-card-body">
                        <div class="d-flex justify-content-center align-items-center">
                            <img src="{{asset('images/clientele-carousel/pharmaceuticals-industry.svg')}}" alt="pharmaceutical client">
                        </div>
                        <div class="cl-info">
                            <h5 class="text-center">Pharmaceuticals Industry</h5>
                        </div>
                    </div>
                </div>
                <div class="cl-card">
                    <div class="cl-card-body">
                        <div class="d-flex justify-content-center align-items-center">
                            <img src="{{asset('images/clientele-carousel/catering-and-food-processing-industries.jpg')}}" alt="catering and food processing client">
                        </div>
                        <div class="cl-info">
                            <h5 class="text-center">Catering & food processing industry</h5>
                        </div>
                    </div>
                </div>
                <div class="cl-card">
                    <div class="cl-card-body">
                        <div class="d-flex justify-content-center align-items-center">
                            <img src="{{asset('images/clientele-carousel/fabrications-and-manufacturers.jpg')}}" alt="fabrications and manufacturers client">
                        </div>
                        <div class="cl-info">
                            <h5 class="text-center">Fabrication and manufacturers</h5>
                        </div>
                    </div>
                </div>
                <div class="cl-card">
                    <div class="cl-card-body">
                        <div class="d-flex justify-content-center align-items-center">
                            <img src="{{asset('images/clientele-carousel/steel-manfacturers.svg')}}" alt="steel manfacturing client">
                        </div>
                        <div class="cl-info">
                            <h5 class="text-center">Steel manufacturers</h5>
                        </div>
                    </div>
                </div>
                <div class="cl-card">
                    <div class="cl-card-body">
                        <div class="d-flex justify-content-center align-items-center">
                            <img src="{{asset('images/clientele-carousel/packaged-drinking-manufactures.svg')}}" alt="packaged drinking manufactures client">
                        </div>
                        <div class="cl-info">
                            <h5 class="text-center">Packaged drinking manufacturers</h5>
                        </div>
                    </div>
                </div>
                <div class="cl-card">
                    <div class="cl-card-body">
                        <div class="d-flex justify-content-center align-items-center">
                            <img src="{{asset('images/clientele-carousel/financial-broking-firms.png')}}" alt="financial broking firms clients">
                        </div>
                        <div class="cl-info">
                            <h5 class="text-center">Finance Broking Firm</h5>
                        </div>
                    </div>
                </div>
                <div class="cl-card">
                    <div class="cl-card-body">
                        <div class="d-flex justify-content-center align-items-center">
                            <img src="{{asset('images/clientele-carousel/doctors-and-medical-professionals.png')}}" alt="doctors and medical professionals clients">
                        </div>
                        <div class="cl-info">
                            <h5 class="text-center">Doctors and medical professional</h5>
                        </div>
                    </div>
                </div>
                <div class="cl-card">
                    <div class="cl-card-body">
                        <div class="d-flex justify-content-center align-items-center">
                            <img src="{{asset('images/clientele-carousel/builders-and-developers.jpg')}}" alt="builders and developers clients">
                        </div>
                        <div class="cl-info">
                            <h5 class="text-center">Builders & Developers</h5>
                        </div>
                    </div>
                </div>
                <div class="cl-card">
                    <div class="cl-card-body">
                        <div class="d-flex justify-content-center align-items-center">
                            <img src="{{asset('images/clientele-carousel/construction-industry.jpg')}}" alt="construction industry clients">
                        </div>
                        <div class="cl-info">
                            <h5 class="text-center">Construction Industry</h5>
                        </div>
                    </div>
                </div>
                <div class="cl-card">
                    <div class="cl-card-body">
                        <div class="d-flex justify-content-center align-items-center">
                            <img src="{{asset('images/clientele-carousel/ship-designer-and-builders.jpg')}}" alt="ship designer and builders clients">
                        </div>
                        <div class="cl-info">
                            <h5 class="text-center">Ship Designer and Builder</h5>
                        </div>
                    </div>
                </div>
                <div class="cl-card">
                    <div class="cl-card-body">
                        <div class="d-flex justify-content-center align-items-center">
                            <img src="{{asset('images/clientele-carousel/law-and-advocates.jpg')}}" alt="law and advocates clients">
                        </div>
                        <div class="cl-info">
                            <h5 class="text-center">Lawyers and Advisory</h5>
                        </div>
                    </div>
                </div>
                <div class="cl-card">
                    <div class="cl-card-body">
                        <div class="d-flex justify-content-center align-items-center">
                            <img src="{{asset('images/clientele-carousel/educational-institutes.jpg')}}" alt="educational institute clients">
                        </div>
                        <div class="cl-info">
                            <h5 class="text-center">Educational Institutions</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="office-space-section">
        <div class="container">
            <div class="office-space-title-container">
                <h2 id="office-space-title" class="text_secondary text-center pt-4">
                    Our Office Space
                </h2>
            </div>
            <div id="office-space-owl-carousel" class="owl-carousel mt-5">
                <img src="{{asset('images/office-space-carousel/img1.jpg')}}" alt="office image 1" class="office-image">
                <img src="{{asset('images/office-space-carousel/img2.jpg')}}" alt="office image 2" class="office-image">
                <img src="{{asset('images/office-space-carousel/img3.jpg')}}" alt="office image 3" class="office-image">
                <img src="{{asset('images/office-space-carousel/img4.jpg')}}" alt="office image 4" class="office-image">
                <img src="{{asset('images/office-space-carousel/img5.jpg')}}" alt="office image 5" class="office-image">
                <img src="{{asset('images/office-space-carousel/img6.jpg')}}" alt="office image 6" class="office-image">
                <img src="{{asset('images/office-space-carousel/img7.jpg')}}" alt="office image 7" class="office-image">
                <img src="{{asset('images/office-space-carousel/img8.jpg')}}" alt="office image 8" class="office-image">
                <img src="{{asset('images/office-space-carousel/img9.jpg')}}" alt="office image 9" class="office-image">
            </div>
        </div>
    </div>
    <div id="contact-us-section">
        <div class="container">
            <div class="contact-us-title-container">
                <h2 id="contact-us-title" class="text_secondary text-center pt-4">
                    Contact Us
                </h2>
            </div>
            <div class="row mt-5">
                <div class="col-md-6">
                    <div class="map-container">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d471.32196520597995!2d72.91262152267734!3d19.08238309194868!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x490e378be5fc1ec6!2sShankar%20Sadan!5e0!3m2!1sen!2sin!4v1655114549389!5m2!1sen!2sin" width="100%" height="450rem" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="send-us-message-container">
                        <form action="{{route('message.store')}}" method="post" id="send-us-message-form" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-form-label" for="">Name</label>
                                        <input type="text" class="form-control" placeholder="John Doe" id="name" name="name">
                                        @error('name')
                                            <label for="" class="error">{{$message}}</label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-form-label" for="">Email</label>
                                        <input type="text" class="form-control" placeholder="johndoe@gmail.com" id="email" name="email">
                                        @error('email')
                                            <label for="" class="error">{{$message}}</label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12 mt-3">
                                    <div class="form-group">
                                        <label class="col-form-label" for="">Subject</label>
                                        <input type="text" class="form-control" placeholder="Subject of message" id="subject" name="subject">
                                        @error('subject')
                                            <label for="" class="error">{{$message}}</label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-12 mt-3">
                                    <div class="form-group">
                                        <label for="" class="col-form-label">Message</label>
                                        <textarea class="form-control" rows="3"
                                                  spellcheck="false" style="resize: none"
                                                  id="message" name="message"
                                        ></textarea>
                                        @error('message')
                                            <label for="" class="error">{{$message}}</label>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 mt-3">
                                    <div class="form-group">
                                        <label class="col-form-label d-block" for="">Want to apply for job?</label>
                                        <div class="form-check d-inline-block apply-job">
                                            <input class="form-check-input" type="radio" name="apply-job-input" id="yes-to-apply-job-input">
                                            <label class="yes-to-apply-job-label" for="yes-to-apply-job-input">
                                                Yes
                                            </label>
                                        </div>
                                        <div class="form-check d-inline-block apply-job">
                                            <input class="form-check-input" type="radio" name="apply-job-input" id="no-to-apply-job-input" checked>
                                            <label class="no-to-apply-job-label" for="no-to-apply-job-input">
                                                No
                                            </label>
                                        </div>
                                        <input type="hidden" name="apply-job" value="0">
                                    </div>
                                </div>
                                <div class="col-md-6 mt-3">
                                    <div class="form-group">
                                        <label class="col-form-label" for="">Enter your Resume</label>
                                        <input type="file" class="form-control" id="resume-input" name="resume" disabled>
                                    </div>
                                </div>
                                <div class="col-md-12 mt-3">
                                    <button type="submit" class="btn btn_primary">Send Message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a href="http://wa.me/+919769282821" class="btn btn_primary whatsapp-btn px-2 py-1 rounded-circle"><span class="bi bi-whatsapp" style="font-size: 1.5rem;"></span></a>
</body>
<footer>
    <div class="upper-footer bg_white">
        <div class="container">
            <div class="row mt-5 pb-5">
                <div class="col-md-6 mt-5">
                    <h5 class="upper-footer-title text_secondary">
                        <strong>Get in Touch</strong>
                    </h5>
                    <div class="mt-4" style="color: #000">
                        <div class="mt-2">
                            <span class="small text_secondary">Location - </span>
                            <span class="">
                                12, A-403, Shankar Sadan, 90 Feet Road, Pant Nagar, Ghatkopar East, Mumbai - 400 075
                            </span>
                        </div>
                        <div class="mt-2">
                            <span class="small text_secondary">Email - </span>
                            <span class="">
                                info@baac.in / ajay@baac.in
                            </span>
                        </div>
                        <div class="mt-2">
                            <span class="small text_secondary">Website - </span>
                            <span class="">
                                www.baac.in
                            </span>
                        </div>
                        <div class="mt-2">
                            <span class="small text_secondary">Phone - </span>
                            <span class="">
                                +91 9769282821
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mt-5">
                    <h5 class="upper-footer-title text_secondary">
                        <strong>Social Links</strong>
                    </h5>
                    <div class="mt-4" style="color: #000">
                        <a class="btn btn_primary rounded-circle mr-2 py-2 social-link" href="https://www.linkedin.com/in/ca-ajay-bhagat-7ab61b1b4/">
                            <span class="bi bi-linkedin"></span>
                        </a>
                        <a class="btn btn_primary rounded-circle mr-2 py-2 social-link" href="http://wa.me/+919769282821">
                            <span class="bi bi-whatsapp"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="lower-footer bg_secondary text_white text-center py-3">
        <span>&copy; Copyright Bhagat Ajay & Co. All Rights Reserved</span>
        <small class="d-block mt-2">Designed by Riddhesh Shah & Soham Karalkar</small>
    </div>
</footer>
<script src="{{asset('vendor/bootstrap-5.0.2-dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('vendor/jQuery/jquery.min.js')}}"></script>
<script src="{{asset('vendor/owl-carousel/dist/owl.carousel.min.js')}}"></script>
<script src="{{asset('vendor/isotope/isotope.min.js')}}"></script>
<script src="{{asset('vendor/waypoints/lib/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('vendor/jquery-validate/jquery-validate.min.js')}}"></script>
<script src="{{asset('vendor/jquery-validate/additional-methods.min.js')}}"></script>
<script src="{{asset('js/scripts.js')}}"></script>
</html>
