<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('users.index');

\Illuminate\Support\Facades\Auth::routes();


Route::middleware('auth')->group(function() {

    Route::get(
        "/admin/get/client/messages",
        [\App\Http\Controllers\Admin\AdminController::class, "getClientMessages"]
    )->name("admin.getClientMessages");

    Route::get(
        "/admin/get/workWithUs/messages",
        [\App\Http\Controllers\Admin\AdminController::class, "getWorkWithUsMessages"]
    )->name("admin.getWorkWithUsMessages");

    Route::get("download/{file_name}", [\App\Http\Controllers\Admin\AdminController::class, "download"])->name("admin.download");
    Route::resource('admin', \App\Http\Controllers\Admin\AdminController::class);

});

Route::resource('message', \App\Http\Controllers\MessageController::class);
