<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Ajay Bhagat',
            'email' => 'ajay@baac.in',
            'password' => Hash::make('ajay@123'),
            'type' => 'admin'
        ]);

        User::create([
            'name' => 'BAAC Staff 1',
            'email' => 'staff1@baac.in',
            'password' => Hash::make('staff1@123'),
            'type' => 'staff'
        ]);

        User::create([
            'name' => 'BAAC Staff 2',
            'email' => 'staff2@baac.in',
            'password' => Hash::make('staff2@123'),
            'type' => 'staff'
        ]);
    }
}
