<?php

namespace Database\Seeders;

use App\Models\Message;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Message::create([
            'name' => 'Riddhesh Shah',
            'email' => 'riddhesh@baac.in',
            'subject' => 'Random Message',
            'message' => 'This is a random message',
            'resume' => null,
        ]);

        Message::create([
            'name' => 'Soham Karalkar',
            'email' => 'soham@baac.in',
            'subject' => 'Random Message',
            'message' => 'This is a random message',
            'resume' => null,
        ]);

        Message::create([
            'name' => 'Krishna Chhabria',
            'email' => 'krishna@baac.in',
            'subject' => 'Random Message',
            'message' => 'This is a random message',
            'resume' => null,
        ]);

        Message::create([
            'name' => 'Shraddha Keniya',
            'email' => 'shraddha@baac.in',
            'subject' => 'Random Message',
            'message' => 'This is a random message',
            'resume' => null,
        ]);
    }
}
