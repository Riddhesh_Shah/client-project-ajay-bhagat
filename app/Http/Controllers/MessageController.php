<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageRequest;
use App\Models\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{

    public function index()
    {
        return view('register.index');
    }

    public function store(MessageRequest $request)
    {
        $data = [
            "name" => $request->name,
            "email" => $request->email,
            "subject" => $request->subject,
            "message" => $request->message,
            "resume" => $request->has('resume') ? explode("/", $request->file('resume')->store('resumes', 'public'))[1] : "",
        ];

        Message::create($data);

        return redirect(route('users.index'))->with("Successfully submitted the message");
    }
}
