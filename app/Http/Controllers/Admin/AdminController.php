<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Message;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class AdminController extends Controller
{
    public function index()
    {
//        dd();
        $workWithUsMessages = Message::query()->where("resume", "=", null) ->latest()->paginate(6);
        return view("admin.index");
    }

    public function getClientMessages(): JsonResponse
    {
        $clientMessages = Message::query()
            ->where("resume", "=", null)
            ->latest()
            ->get(["name", "subject", "email", "message"]);
        return response()->json(["data" => $clientMessages]);
    }

    public function getWorkWithUsMessages(): JsonResponse
    {
        $clientMessages = Message::query()
            ->where("resume", "!=", null)
            ->latest()
            ->get(["name", "subject", "email", "message", "resume"]);
        return response()->json(["data" => $clientMessages]);
    }

    public function download(Request $request): Response
    {
        $path = Storage::disk('public')->path("resumes/".$request->file_name);
        $content = file_get_contents($path);
        return response($content)->withHeaders([
            'Content-type' => mime_content_type($path)
        ]);
    }
}
