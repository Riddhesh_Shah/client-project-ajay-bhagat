# Bhagat Ajay & Company

## Description
This is a Live Project, website has been created according to client's requirement

## Output Screens

### Landing Screen
![img.png](img.png)

### About Us
![img_1.png](img_1.png)

### Our Services
![img_2.png](img_2.png)

### Our Clientele
![img_3.png](img_3.png)

### Contact Us
![img_4.png](img_4.png)
